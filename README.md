# Issue #2044

A test case that demonstrates OpenCPI Issue #2044.

This issue relates to `hdlconfig` xmls.

This project is comprised of:
 * The standard OSP for the ZCU104.
 * A config called `broken_config.xml` which is representative of the `data_sink_qdac_asm` container for the ZCU104.
